# Maple

A demonstrator for basic embedded programming in C++ through mbed-os, showing how to build a control system.


## Beschreibung

Im Rahmen des Projektes Maple wird ein Demonstrator aufgebaut, anhand dessen
die Inhalte der Module Regelungstechnik und Informatik nachvollzogen werden
können.  

Das wird erreicht, indem ein Regelkreis aus einfachen Komponenten aufgebaut
und in der Programmiersprache C++ von einem Mikrocontroller geregelt wird.
Durch die Offenlegung der Quellcodes der Programme sollen die wesentlichen
Inhalte und Paradigmen der Objekt-Orientierten Programmierung vermittelt
werden.


Das Projekt besteht zu einem großen Teil aus Software, die auf einem
Mikrocontroller ausgeführt wird. Im Rahmen des Projektes kommt zu diesem 
Zweck ein **[STM32F446RE](https://www.st.com/en/microcontrollers-microprocessors/stm32f446re.html)** 
der Firma *STMicroelectronics* zum Einsatz.  
Die Programmierung erfolgt in C/C++ über die Softwareumgebung und das
Framework von [mbed OS](https://www.mbed.com/en/platform/mbed-os/).