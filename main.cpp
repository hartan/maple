/**
 * @file main.cpp
 * @author Andreas Hartmann
 * @brief Build the control system for maple.
 * @version 0.1
 * @date 2019-08-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "mbed-os/mbed.h"
#include "include/maple.h"
#include "include/maple_ui.h"
#include "ISensor.h"
#include "IRegler.h"
#include "ISteller.h"
//! Weitere Includes hier
//! z.B. fuer unseren (speziellen) Sensor, Regler, etc.
//! und das Benutzer Interface.

#define PERIODENDAUER   0.001  // Periodendauer in s


/* Wir benutzen den Ticker, um den eigentlichen Regelkreis in fest definierten
 * Zeitabstaenden aufzurufen. Dadurch koennen wir vernuenftig mit D und I
 * Reglern arbeiten.
 */
Ticker tick;
/* Der Thread kuemmert sich um das Benutzer Interface und arbeitet vollstaendig
 * im Hintergrund. So lange der Regelkreis grade also nicht arbeitet (siehe
 * Periodendauer) kann sich der Mikrocontroller um das Benutzerinterface
 * kuemmern.
 * 
 * Diese herangehensweise ist deshalb notwendig, weil wir nur ueber einen
 * Kern verfuegen und damit keine echte Parallelitaet erreichen!
 */
Thread ux(osPriorityNormal, OS_STACK_SIZE, NULL, "Benutzerinterface");


int main() {
    // Hier erzeugen wir die Pointer auf die Interfaces.
    ISensor *sensor;
    IRegler *regler;
    ISteller *steller;

    //! Definiere hier: Sensor, Regler, Steller, UI !//
    /* ----- Unser Abstandssensor ------------------ */
    sensor = /* Sinnvoller Code hier */
    /* ----- Unser PID-Regler ---------------------- */
    regler = 
    /* ----- Unser Steller ------------------------- */
    steller =
    /* ----- Unser UI ------------------------------ */
    Maple_UI ui();


    Maple maple(sensor, regler, steller, PERIODENDAUER);

    /* Hier "haengen" wir die Maple Klasse an das erstellte Ticker Objekt an.
     * Dadurch weiss der Ticker für die gesamte Laufzeit des Programms, wen
     * und was er regelmaessig aufzurufen hat.
     */
    tick.attach(callback(&maple, &Maple::regelkreis), PERIODENDAUER);
    /* Hier starten wir den Thread fuer das UI. Auch dem Thread geben wir eine
     * Referenz auf die auszufuehrende Funktion (hier: Methode) mit.
     */
    ux.start(callback(&ui, &Maple_UI::handle_input));

    // Alle relevanten Dinge laufen im Hintergrund, in main() gibts es daher
    // nichts mehr zu tun.
    while (1) {}
}