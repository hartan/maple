/**
 * @file IUI.h
 * @author Andreas Hartmann
 * @brief An interface class for the user interface.
 * @version 0.1
 * @date 2019-08-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */


class IUI {
public:
    virtual void handle_input() = 0;
};