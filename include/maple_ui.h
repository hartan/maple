/**
 * @file maple_ui.h
 * @author Andreas Hartmann
 * @brief UI for the first revision of the maple project.
 * @version 0.1
 * @date 2019-08-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef MAPLE_UI
#define MAPLE_UI 1


#include "mbed-os/mbed.h"
#include "IUI.h"
#include "libs/QEI/QEI.h"
#include "libs/LCD/TextLCD.h"


/**
 * @brief Erledigt die vollstaendige Benutzerinteraktion
 * 
 * Wir verwenden ein 4x20 Zeichen LC-Display und 4 Drehgeber, um folgende 4
 * Parameter zu variieren:
 * - K_p 
 * - K_i
 * - K_d
 * - w (Fuehrungsgroesse)
 * 
 * Alle der Parameter werden mit der @ref Maple Klasse geteilt. Daher koennen
 * wir nur mit Zeigern oder Referenzen arbeiten.
 * 
 * Die Signale der Drehgeber werden in Interrupts verarbeitet. Dadurch wird
 * sicher gestellt, dass wir moeglichst keinen Impuls verpassen und die
 * Bedienung fluessig laeuft.
 */
class Maple_UI : public IUI {
public:
    /**
     * @brief Construct a new Maple_UI object
     * 
     * Default Konstruktor.
     */
    Maple_UI();
    
    /**
     * @brief Interface Methode fuer die Verwaltung des UI.
     * 
     * Kuemmert sich um die Wertzuweisungen und das aktualisieren des Displays.
     */
    void handle_input();

private:
    /**
     * @brief Interrupt handler fuer die Drehgeber.
     */
    void rot_enc_interrupt();

    QEI Test1;
    TextLCD _lcd;
};

#endif /* MAPLE_UI */