/**
 * @file maple.h
 * @author Andreas Hartmann
 * @brief Header zur Maple Klasse
 * @version 0.1
 * @date 2019-08-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef MAPLE_CLASS
#define MAPLE_CLASS

#include "ISensor.h"
#include "IRegler.h"
#include "ISteller.h"


/**
 * @brief Die Maple Klasse
 * 
 * Die Klasse verwaltet den vollstaendigen Zugriff auf alle Komponenten des
 * Regelkreises. Sie kuemmert sich also um die Beschaffung der Messwerte,
 * die Berechnung der Reglerausgangsgroesse und die Ansteuerung des
 * Stellgliedes.
 * 
 * Da alle Komponenten ein einheitliches Interface haben und von den
 * jeweiligen Interfaceklassen abgeleitet sind, verwalten wir lediglich
 * Pointer auf die Interfaces zu den Klassen.
 * 
 * Das hat fuer uns den Vorteil, dass wir uns keine Gedanken um die
 * Implementierung machen muessen und einzelne Bestandteile einfach
 * austauschen koennen.
 */
class Maple {
public:
    /**
     * @brief Construct a new Maple object
     * 
     * Default Konstruktor
     */
    Maple();

    /**
     * @brief Construct a new Maple object
     * 
     * @param sensor Ein Zeiger auf den zu verwendenden Sensor
     * @param regler Ein Zeiger auf den zu verwendenden Regler
     * @param steller Ein Zeiger auf den zu verwendenden Steller
     * @param periodendauer Die Periodendauer des Regelkreises in Sekunden
     */
    Maple(ISensor *sensor, IRegler *regler, ISteller* steller,
        unsigned int periodendauer);

    /**
     * @brief Destroy the Maple object
     */
    ~Maple();


    /**
     * @brief Lege den zu verwendenden Sensor fest.
     * 
     * @param sensor Ein Zeiger auf den zu verwendenden Sensor.
     * 
     * @return void
     */
    void setze_sensor(ISensor *sensor);

    /**
     * @brief Lege den zu verwendenden Regler fest.
     * 
     * @param regler Ein Zeiger auf den zu verwendenden Regler.
     * 
     * @return void
     */
    void setze_regler(IRegler *regler);

    /**
     * @brief Lege den zu verwendenden Steller fest.
     * 
     * @param strecke Ein Zeiger auf den zu verwendenden Steller.
     * 
     * @return void
     */
    void setze_steller(IStrecke *strecke);

    /**
     * @brief Lege die Samplezeit (Periodendauer) fuer den Regelkreis fest.
     * 
     * Da Maple den ganzen Regelkreis verwaltet ist es notwendig, dass die
     * fundamentale Samplezeit bekannt ist, da sich I- und D-Regler sonst
     * nicht wie erwartet verhalten.
     * 
     * @param zeit Die Periodendauer in Sekunden.
     */
    void setze_periodendauer(unsigned int periodendauer);

    /**
     * @brief Fuehrt den Regelkreis aus.
     * 
     * Das Herzstueck unseres Regelkreises. Diese Methode holt die Sensorwerte,
     * berechnet die Reglerausgangsgroesse und steuert den Steller an.
     */
    void regelkreis();

private:
    ISensor *_sensor;
    IRegler *_regler;
    ISteller *_steller;
    unsigned int _periodendauer;
};

#endif /* MAPLE_CLASS */
