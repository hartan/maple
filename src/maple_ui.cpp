/**
 * @file maple_ui.cpp
 * @author Andreas Hartmann
 * @brief Source Codes zur Maple_UI Klasse
 * @version 0.1
 * @date 2019-08-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "mbed-os/mbed.h"
#include "../include/maple_ui.h"
#include "../libs/LCD/TextLCD.h"
#include "../libs/QEI/QEI.h"

/* Pin Namen siehe hier: https://os.mbed.com/platforms/ST-Nucleo-F446RE/ */
#define CHANNEL_A       PA_3  // D0 am Arduino Header
#define CHANNEL_B       PA_2  // D1
#define PULSES_PER_REV  128   //! Bitte anpassen!

#define I2C_SDA         PB_9  // I2C_SDA am Arduino Header
#define I2C_SCL         PB8   // I2C_SCL
#define DISPLAY_ADDRESS 0x27  // oder 0x3F, Siehe Anleitung zum Display


Maple_UI::Maple_UI()
{
    // Initialisiere die Drehgeber.
    Test1 = QEI(CHANNEL_A, CHANNEL_B, 0, PULSES_PER_REV);
    I2C i2c_lcd(I2C_SDA, I2C_SCL);

    _lcd = TextLCD_I2C(&i2c_lcd, DISPLAY_ADDRESS, TextLCD::LCD20x4, TextLCD::HD44780);
    // Cursor abschalten
    _lcd.setCursor(CurOff_BlkOff);
    _lcd.setMode(DispOn);
    _lcd.setBacklight(LightOn);

}

void Maple_UI::handle_input() 
{
    // 1.) Aktuelle Werte der Drehgeber auslesen
    int pulses = QEI.getPulses();
    float k_p = 0.354;
    float k_i = 2.76;
    float k_d = 56.823;
    // 2.) Werte logarithmisch umrechnen
    // 3.) Werte zuweisen
    // 4.) Display aktualisieren
    _lcd.cls();
    _lcd.locate(0, 0);
    _lcd.printf("Drehgeber 1: %8d\n", pulses);
    _lcd.locate(0, 1);
    _lcd.printf("Kp = %2.3f\n", k_p);
    _lcd.locate(0, 2);
    _lcd.printf("Ki = %2.3f\n", k_i);
    _lcd.locate(0, 3);
    _lcd.printf("Kd = %2.3f\n", k_d);
}
