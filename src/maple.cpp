/**
 * @file maple.cpp
 * @author Andreas Hartmann
 * @brief Source codes to the maple class.
 * @version 0.1
 * @date 2019-08-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "../include/maple.h"


// Da der default Konstruktor in unserem Fall keine sinnvollen Dinge tut,
// ueberlassen wir dem Compiler die Generierung des Codes.
Maple::Maple() 
{
}


/* An dieser Stelle verwenden wir ein interessantes Konzept zur Initialisierung:
 * Wir lassen uns die member Variablen (_sensor etc.) direkt mit den richtigen
 * Werten erzeugen!
 * 
 * Wuerden wir stattdessen im Koerper des Konstruktors eine einfache Zuweisung
 * durchfuehren, wuerden die Objekte erst mit default Werten erzeugt, nur um
 * diese Werte anschliessend zu ueberschreiben. Das ist hier nicht noetig.
 */
Maple::Maple(ISensor *sensor, IRegler *regler, ISteller* steller,
    unsigned int periodendauer) :
        _sensor(sensor),    // Erzeugt das member _sensor und weist direkt den Wert (sensor) zu.
        _regler(regler),    // Hier das selbe fuer den Regler.
        _steller(steller),   // Wichtig ist der Doppelpunkt direkt vor "{"
        _periodendauer(periodendauer)
{
    // Der Koerper bleibt leer, die Initialisierung ist ja schon geschehen.
}


void Maple::setze_sensor(ISensor *sensor) 
{
    _sensor = sensor;
    return;
}


void Maple::setze_regler(IRegler* regler)
{
    _regler = regler;
    return;
}


void Maple::setze_steller(ISteller *steller)
{
    _steller = steller;
    return;
}


void Maple::setze_samplezeit(unsigned int periodendauer)
{
    _periodendauer = periodendauer;
    return;
}


void Maple::regelkreis()
{
    try
    {
        // Bilde die Regeldifferenz
        double e = _w - _sensor.lesen();
        double y_r = _regler.regle(e);
        double _steller.stelle(y_r);
        // Oder auch:
        // _steller.stelle(_regler.regle(w - _sensor-lesen()))
    }
    catch(const std::exception& e)
    {
        error(e.what());
    }
    
}